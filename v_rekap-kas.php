<?php
session_start();
if($_SESSION['id_role']==""){
	header("location:index?pesan=gagal");
}
if ( !isset($_SESSION['username'])) {
	header("location:index");
}
include 'conf/model.php';
$model = new Model();
$page="rekap-kas";
?>
<!doctype html>
<html lang="en">
<head>
	<title>Add kas</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="assets/vendor/chartist/css/chartist-custom.css">
	<link rel="stylesheet" href="assets-login/css/sweetalert2.min.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<script src="https://kit.fontawesome.com/45da9d5b26.js" crossorigin="anonymous"></script>
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
    <?php include "templates/navbar.php"; ?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include "templates/sidebar.php"; ?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
          <!-- OVERVIEW -->
            <div class="panel-body">
            <div class="panel">
				<div class="panel-heading">
                  <h3 class="panel-title">Table Add Kas</h3>
                  <div class="right">
					<button type="button" class="btn" data-toggle="modal" data-target="#newMenuModal"><i class="lnr lnr-plus-circle"></i></button>
				  </div>
				</div>
				<div class="panel-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>No</th>
					<!--  <th>id</th> -->
                          <th>Name</th>
						  <th>Total</th>
                          <th>Date</th>
						  <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
					  <?php
					  $result = $model->show();
						$no = 1;
						if (!empty($result)) {
                        	foreach($result as $data) :
                          ?>
                          <tr>
                            <td><?php echo $no++ ?></td>
					   		<td><?php echo $d['id_kas']; ?></td>
                            <td><?php echo $data->nama ?></td>
                            <td><?php echo $data->jumlah ?></td>
							<td><?php echo $data->tanggal ?></td>
							<td>
							<a href="v_edit_rekap?id=<?php echo $data->id_kas ?>" class="btn btn-primary">Edit</a>
							<a href="act/p_delete_rkp?id=<?php echo $data->id_kas ?>" class="btn btn-danger">Delete</a>
							</td>
                          </tr>
                        <?php
						endforeach;
					} else {
                        ?>
						<td>Data Kosong</td>
					<?php } ?>
                      </tbody>
                    </table>
                  </div>

				</div>
			</div>
            </div>
          </div>
        </div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
    <?php include "templates/footer.php"; ?>
  </div>
  <!-- Modal -->
  <!-- MODAL INPUT  -->
  		<div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title" id="newMenuModalLabel">Add Payments</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="act/p_add_rkp?action=add" method="POST" onSubmit="validasi()">
                            <div class="modal-body">
							<div class="alert alert-warning" role="alert">
							before entering data, first check the data you entered is correct or wrong!
							</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Full Name :</label>
										<input type="username" name="nama" id="nama" class="form-control" aria-describedby="emailHelp" placeholder="Enter Full Name">
										<small id="emailHelp" class="form-text text-muted">input data accordingly !</small>
									</div>
									<div class="form-group">
										<label for="exampleInputPassword1">Total :</label>
										<input type="total" class="form-control" name="jumlah" id="jumlah" placeholder="Input Total" onkeypress="return justNumber(event)" maxlength="6">
										<small id="emailHelp" class="form-text text-muted">input data accordingly !</small>
									</div>
                                    <br>
									<div class="form-group">
										<label for="exampleInputPassword1">Date :</label>
										&nbsp;
										<input type="date" name="tanggal" id="tanggal">
										&nbsp;
										<small class="form-text text-muted">input data accordingly !</small>
									</div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" name="add" class="btn btn-primary">add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END MODAL INPUT  -->
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets-login/js/sweetalert2.all.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="assets/vendor/chartist/js/chartist.min.js"></script>
	<script src="assets/scripts/klorofil-common.js"></script>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
	function validasi()
	{
		var nama =	document.getElementById("nama").value;
		var jumlah = document.getElementById("jumlah").value;
		var tanggal = document.getElementById("nama").value;

		if (nama !="" && jumlah !="" && tanggal !="") {
			return true;
		} else {
			alert('first fill in the available fields!');
		}
	}

	function justNumber(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))

		    return false;
		  return true;
		}

	$( function() {
    $( "#date" ).datepicker({
      dateFormat: "yy-mm-dd"
    });
  } );
	</script>
</body>

</html>
