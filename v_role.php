<?php
session_start();	
include 'conf/model_role.php';
if($_SESSION['id_role']==""){
	header("location:index?pesan=gagal");
}
if ( !isset($_SESSION['username'])) {
	header("location:index");
}
$model = new Model_Role();
$page = "v-role";
?>
<!doctype html>
<html lang="en">
<head>
	<title>Users</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="assets/vendor/chartist/css/chartist-custom.css">
	<link rel="stylesheet" href="assets-login/css/sweetalert2.min.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<script src="https://kit.fontawesome.com/45da9d5b26.js" crossorigin="anonymous"></script>
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include 'templates/navbar.php'; ?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include "templates/sidebar.php"; ?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
          <!-- OVERVIEW -->
          <div class="panel">
            <div class="panel-heading">
			  <h3 class="panel-title">User Management</h3>
			  	<div class="right">
					<button type="button" class="btn" data-toggle="modal" data-target="#newMenuModal"><i class="lnr lnr-plus-circle"></i></button>
				</div>
            </div>
            <div class="panel-body">
			<div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <th>No</th>
					<!--  <th>id</th> -->
                          <th>Role</th>
						  <th>Active</th>
                    <!-- <th>Date</th> -->
						  <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
					  $result = $model->show();
						$no = 1;
						if (!empty($result)) {
                        	foreach($result as $data) :
						  ?>
						  <tr>
						  <td><?php echo $no++ ?></td>
                            <td><?php echo $data->name_role ?></td>
                            <td><?php echo $data->is_active ?></td>
							<td>
							<a href="act/p_delete_rl?id=<?php echo $data->id_role ?>" class="btn btn-danger">Delete</a>
							</td>
                          </tr>
						  <?php
						endforeach;
					} else {
                        ?>
						<td>Data Kosong</td>
					<?php } ?>
                      </tbody>
                    </table>
                  </div>
            </div>
          </div>
        </div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
    <?php include "templates/footer.php"; ?>
	</div>
	<!-- END WRAPPER -->
	<!-- MODAL INPUT  -->
	<div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title" id="newMenuModalLabel">Add Role</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="act/p_add_rl?action=add" method="POST" onSubmit="validasi()">
                            <div class="modal-body">
							<div class="alert alert-warning" role="alert">
							this data will be stored securely!!
							</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Name Role :</label>
										<input type="username" name="nama" id="nama" class="form-control" aria-describedby="emailHelp" placeholder="Enter Role Name">
										<small id="emailHelp" class="form-text text-muted">input data accordingly !</small>
									</div>
                                    <br>
									<div class="form-group">
									<label for="exampleFormControlSelect1">Choose Active :</label>
									<select class="form-control" name="active" id="exampleFormControlSelect1">
									<option value="1">Active</option>
									<option value="0">Non-Active</option>
									</select>
									<small id="emailHelp" class="form-text text-muted">input data accordingly !</small>
								</div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" name="add" class="btn btn-primary">add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END MODAL INPUT  -->
	<!-- Javascript -->
	<script src="assets-login/js/jquery-3.5.1.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.js"></script>
	<script src="assets-login/js/sweetalert2.all.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="assets/vendor/chartist/js/chartist.min.js"></script>
	<script src="assets/scripts/klorofil-common.js"></script>
</body>

</html>