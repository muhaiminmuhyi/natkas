<?php
session_start();	
$page = "dashboard";
include "conf/func.php";

if($_SESSION['id_role']==""){
	header("location:index?pesan=gagal");
}

if ( !isset($_SESSION['username'])) {
	header("location:index");
}


?>
<!doctype html>
<html lang="en">
<head>
	<title>Dashboard</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="assets/vendor/chartist/css/chartist-custom.css">
	<link rel="stylesheet" href="assets-login/css/sweetalert2.min.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<script src="https://kit.fontawesome.com/45da9d5b26.js" crossorigin="anonymous"></script>
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/v.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/v.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include 'templates/navbar.php'; ?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include "templates/sidebar.php"; ?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
          <!-- OVERVIEW -->
          <div class="panel">
            <div class="panel-heading">
              <h3 class="panel-title">Dashboard</h3>
			  <?php 
					if(isset($_GET['pesan'])){
						if($_GET['pesan']=="berhasil-login"){
							echo "<br><div class='alert alert-success' role='alert'>
							Welcome $_SESSION[username], we miss you!
							</div>";
						}
					}
				?>
            </div>
            <div class="panel-body">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates aliquam corrupti amet obcaecati assumenda, suscipit ipsum! Assumenda, laboriosam nisi. Deserunt, qui adipisci deleniti vero nesciunt ut, asperiores facere tempore eligendi mollitia voluptatum quos ab quia recusandae nemo nisi eaque tempora earum veniam, enim voluptates et. Fugiat sit accusamus reprehenderit ea, incidunt ut natus quam, esse tempore placeat sint velit dolore eius voluptates nesciunt voluptatem debitis harum saepe dicta neque quas labore. Iure eum veniam similique accusamus consequuntur sunt, earum quos obcaecati, ratione incidunt vitae inventore dolor ipsa esse explicabo facere, reprehenderit quibusdam voluptates autem commodi! Earum, sapiente ab. Mollitia, commodi.
            </div>
          </div>
        </div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
    <?php include "templates/footer.php"; ?>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets-login/js/jquery-3.5.1.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
 	<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.js"></script>
	<script src="assets-login/js/sweetalert2.all.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="assets/vendor/chartist/js/chartist.min.js"></script>
	<script src="assets/scripts/klorofil-common.js"></script>
</body>

</html>
