<?php
include 'connection.php';
class Model extends Connection
{
  public function __construct()
  {
     $this->conn = $this->get_connection();
  }

  public function show()
  {
    $sql = "SELECT * FROM tb_kas
            INNER JOIN pekerja ON pekerja.id_pekerja = tb_kas.id_pekerja
            INNER JOIN tb_penambahan_kas ON tb_penambahan_kas.id_tambah_kas = tb_kas.id_tambah_kas 
            INNER JOIN tb_pengeluaran_kas ON tb_pengeluaran_kas.id_pengeluaran_kas = tb_kas.id_pengeluaran_kas
            INNER JOIN tb_kategori ON tb_kategori.id_kategori = tb_kas.id_kategori"; 
    $bind = $this->conn->query($sql);
    while ( $obj = $bind->fetch_object())
    {
      $baris[] = $obj;
    }
    if (!empty($baris)) {
        return $baris;
    }
  }

  public function insert_kas($nama,$tanggal,$jumlah)
  {
    $sql = "INSERT INTO tb_kas 
            (nama, tanggal, jumlah) 
            VALUES 
            ('$nama' ,'$tanggal' ,'$jumlah')";
    
    $query = $this->conn->query($sql);

    if ($query === true) {
      echo "<script>alert('Data was successfully Created!!');window.location='../v_rekap-kas'</script>";
  } else {
      echo "<script>alert('Data failed to Create!!');window.location='../v_rekap-kas'</script>";
  }
  
  }

  public function edit($id_kas)
  {
      $sql = "SELECT * FROM tb_kas WHERE id_kas='$id_kas'";
      $bind = $this->conn->query($sql);
      while ($obj = $bind->fetch_object()) {
      $baris = $obj;
      }
      return $baris;
  }

  public function update_kas($id_kas,$nama,$jumlah,$tanggal)
  {
    $sql = "UPDATE tb_kas SET 
            nama='$nama',
            jumlah='$jumlah',
            tanggal='$tanggal' 
            WHERE 
            id_kas=$id_kas";

    $query = $this->conn->query($sql);

    if (!$query) {
      echo "<script>alert('Data failed to update!!');window.location='../v_rekap-kas'</script>";
    } else {
      echo "<script>alert('Data was successfully updated!!');window.location='../v_rekap-kas'</script>";
    }
    

  }

  public function delete($id_kas)
  {
    $sql = "DELETE FROM tb_kas WHERE id_kas='$id_kas'";

    $query = $this->conn->query($sql);

    if ($query === true) {
      echo "<script>alert('Data was successfully Deleted!!');window.location='../v_rekap-kas'</script>";
    } else {
        echo "<script>alert('Data failed to delete!!');window.location='../v_rekap-kas'</script>";
    }
  
  }
}
?>
