<?php
session_start();
if($_SESSION['id_role']==""){
	header("location:index?pesan=gagal");
}
if ( !isset($_SESSION['username'])) {
	header("location:index");
}
require 'conf/model_user.php';
$id_user = $_GET['id'];
$model = new Model_User();
$data = $model->edit($id_user);
?>
<!doctype html>
<html lang="en">
<head>
	<title>Edit User</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="assets/vendor/chartist/css/chartist-custom.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<script src="https://kit.fontawesome.com/45da9d5b26.js" crossorigin="anonymous"></script>
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include 'templates/navbar.php'; ?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include "templates/sidebar.php"; ?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
          <!-- OVERVIEW -->
          <div class="panel">
            <div class="panel-heading">
              <h3 class="panel-title">Edit</h3>
            </div>
            <div class="panel-body">
              <form action="act/p_update_usr?action=update" method="POST" >
              <input type="hidden" name="id_user" id="id_user" value="<?php echo $data->id_user ?>"/>

              <div class="input-group mb-3">
                <input type="text" class="form-control" name="nama" id="nama" value="<?php echo $data->user ?>" name="nama" aria-describedby="basic-addon1" >
              </div>
              <br>
			  
              <div class="form-group">
                <label for="exampleFormControlSelect1">Choose Level :</label>
                <select class="form-control" name="id_role" id="id_role">
				<option>0</option>
				<option>1</option>
				<option>2</option>
                </select>
              </div>
              <br>
              <br>
              <button type="submit" name="submit-up-user" class="btn btn-primary">UPDATE</button>
			  <button type="button" class="btn btn-secondary" onclick="window.location.href='v_users'">Cancel</button>
              </form>
          </div>
        </div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
    <?php include "templates/footer.php"; ?>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="assets/vendor/chartist/js/chartist.min.js"></script>
	<script src="assets/scripts/klorofil-common.js"></script>
	<script>
	function validasi()
	{
		var nama =	document.getElementById("nama").value;
		var jumlah = document.getElementById("jumlah").value;
		var tanggal = document.getElementById("nama").value;

		if (nama !="" && jumlah !="" && tanggal !="") {
			return true;
		} else {
			alert('first fill in the available fields!');
		}
	}

  function justNumber(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))

		    return false;
		  return true;
		}

	$( function() {
    $( "#date" ).datepicker({
      dateFormat: "yy-mm-dd"
    });
  } );
	</script>
</body>

</html>
