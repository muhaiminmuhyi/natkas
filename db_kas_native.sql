-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 20, 2020 at 02:50 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kas_native`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin_log`
--

CREATE TABLE `tb_admin_log` (
  `id` int(11) NOT NULL,
  `tgl` datetime DEFAULT NULL,
  `expired` datetime DEFAULT NULL,
  `token` varchar(40) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `useragent` varchar(150) DEFAULT NULL,
  `stat` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_admin_log`
--

INSERT INTO `tb_admin_log` (`id`, `tgl`, `expired`, `token`, `username`, `ip`, `useragent`, `stat`) VALUES
(1, '2020-06-24 23:21:37', '2020-06-25 05:21:37', 'babc8fc74f4e15386f0d003455cebe5488f32500', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(2, '2020-06-24 23:40:12', '2020-06-25 05:40:12', 'c034dce766fa0ba8ee95bb882769ea3eef46b7c6', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(3, '2020-06-26 03:42:41', '2020-06-26 03:44:57', '54c0417343fd94671cde48004803bcbaf32123c8', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(4, '2020-06-26 04:06:05', '2020-06-26 04:14:19', 'eff1988752ade052e5129d73300a387b756d5ce7', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(5, '2020-06-26 04:14:28', '2020-06-26 10:14:28', '60e15a7cdb4bd513aac6984c1b997536efa7087d', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(6, '2020-06-26 10:20:22', '2020-06-26 16:20:22', 'd7d4d1a7e4488d05db5c5c32f26546bce191718c', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(7, '2020-06-26 10:22:01', '2020-06-26 16:22:01', 'dca1745526ce0e30cbe7b79af88cf9b27e5a5c25', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(8, '2020-06-26 10:33:09', '2020-06-26 16:33:09', 'b979f6af6df8e0ef980451110e11a0dcca087a1d', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(9, '2020-06-27 01:23:20', '2020-06-27 01:46:30', 'b5df399c0e47881ebb761f10d35b051e8bc74983', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(10, '2020-06-27 01:46:41', '2020-06-27 01:50:33', 'be0e48e73abe48413e0612063ea465b76c944a02', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(11, '2020-06-27 01:50:42', '0000-00-00 00:00:00', '', 'hilih', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 9),
(12, '2020-06-27 01:52:08', '2020-06-27 07:52:08', 'be09ba36b5dbfc64259206b6c8c64d41ef788fac', 'hilih', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(13, '2020-06-27 03:08:33', '2020-06-27 09:08:33', '36b8c84c7ce4ff7aee030a057969a011fae35cd0', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(14, '2020-06-27 03:09:11', '2020-06-27 09:09:11', '54900f90a757a89462436763cfb42f80bc8b1944', 'admin', '::1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/81.0.4044.124 Mobile/15E148 Safari/604.1', 1),
(15, '2020-06-27 03:16:02', '2020-06-27 09:16:02', 'f4db57d8570095c63931c57171614299c5bb923f', 'admin', '::1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/81.0.4044.124 Mobile/15E148 Safari/604.1', 1),
(16, '2020-06-27 07:07:18', '2020-06-27 13:07:18', 'f06bedbb497b4924dcc4387c32d466fed0004a09', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(17, '2020-06-27 07:19:38', '2020-06-27 13:19:38', '3f6068085972d55c081aac814815f7a6d6dde6d4', 'admin', '::1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/81.0.4044.124 Mobile/15E148 Safari/604.1', 1),
(18, '2020-06-27 10:55:06', '0000-00-00 00:00:00', '', '<script>alert(\"Hello\")</script>', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 9),
(19, '2020-06-27 12:39:58', '2020-06-27 18:39:58', '663dc60f44f60efd7f97887ddadcf05b9628eb5d', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(20, '2020-06-27 12:41:09', '2020-06-27 12:58:11', '5221e2c6866b14d24cb8f35f208dba9bd11f493c', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(21, '2020-06-27 12:58:40', '2020-06-27 12:58:43', 'd56c3b726efc060bd55faa41aec223de64c84ba3', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(22, '2020-06-27 12:59:35', '2020-06-27 15:00:08', '460fcca01f0f7332f432dc50218301fe9b73f6db', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(23, '2020-06-27 15:01:03', '2020-06-27 15:02:14', '0b665f9e3f08ea0a256913629afbe75d37f6a120', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(24, '2020-06-27 15:04:39', '2020-06-27 15:04:48', 'fa3d479229615216d71e7645d6d6c0f8330d39da', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(25, '2020-06-27 15:05:52', '2020-06-27 15:12:40', '6adcb647a2c0e1b1064cad285d083f19417ebf82', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(26, '2020-06-27 15:13:10', '2020-06-27 15:19:22', '4e71c4d57788d945ad334b8232ef17e271769c07', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(27, '2020-06-27 15:27:02', '2020-06-27 21:27:02', '654c35347d8fb69b9eef9ad766a4cb27780339f6', 'admin', '::1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4_7 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Mobile/15E148 Safari/604.1', 1),
(28, '2020-06-27 15:28:43', '2020-06-27 15:41:04', 'f953f453cc4495cdbdcff84a3ed419ab9184226d', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(29, '2020-06-27 15:53:52', '2020-06-27 15:55:02', '93a98c342bc552ace803d4dfd2de32893d9ec82f', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(30, '2020-06-27 15:55:47', '2020-06-27 21:55:47', '0765bfae8369cb14a1b71e8c0dee9cfc0a3375c8', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(31, '2020-06-27 15:57:04', '2020-06-27 15:57:33', 'ece5d3ce1c8598c0a221717d70eb88ed9634461a', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(32, '2020-06-29 01:47:49', '2020-06-29 07:47:49', 'bc6885e034f7f7283ec16477d4cc2a5723bbeb46', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(33, '2020-06-29 03:47:38', '2020-06-29 09:47:38', '22b79c5a1f804e22ee981e8d8646a40db37addac', 'hilih', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(34, '2020-06-29 03:55:58', '2020-06-29 09:55:58', '9df9b4d07c80bcf79c209dbbf93e85a6dd37d630', 'admin', '::1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/81.0.4044.124 Mobile/15E148 Safari/604.1', 1),
(35, '2020-06-29 05:50:28', '2020-06-29 11:50:28', 'f6424a128b8fad245a3d6d556bdb31987ce04d91', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 1),
(36, '2020-06-29 12:48:32', '2020-06-29 18:48:32', '7527d2cfc4101c7c0350cacb90dbda170d658087', 'admin', '::1', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36 Edg/83.0.478.50', 1),
(37, '2020-08-03 15:42:19', '2020-08-03 21:42:19', 'd9f5fca474bfeed128c0b317403a0febafe73d6e', 'admin', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36', 1),
(38, '2020-08-03 21:09:31', '2020-08-04 03:09:31', 'a6dbea8dc08689d72798ea61e9d347e3d794c304', 'admin', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36 Edg/84.0.522.52', 1),
(39, '2020-08-04 14:06:01', '2020-08-04 20:06:01', 'f0ccd44b3ab53df85bbd95d597f0f33fb9e82dac', 'admin', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36', 1),
(40, '2020-08-08 11:20:39', '2020-08-08 17:20:39', 'b6b86320f3d704a16f4276bf12375590d764859c', 'admin', '::1', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/81.0.4044.124 Mobile/15E148 Safari/604.1', 1),
(41, '2020-08-10 14:08:07', '2020-08-10 20:08:07', 'a4cda3c977ac6b0ab26fad546bc389dd11f54642', 'admin', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36', 1),
(42, '2020-08-25 18:18:50', '2020-08-26 00:18:50', '991107c00eaa0fba72f1087803808242a898b4eb', 'admin', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 1),
(43, '2020-08-26 16:47:39', '2020-08-26 22:47:39', '054b1407439c623f3c91bf9ab3efd6fd1f6e9018', 'admin', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 1),
(44, '2020-09-04 01:12:01', '2020-09-04 07:12:01', '6179adde0e18b2a58cbe648f323b6a7d54dec253', 'admin', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.135 Safari/537.36', 1),
(45, '2020-09-17 05:57:31', '2020-09-17 06:59:21', '86b3b335cf8b492054cf5afc59ba7b3d9ac1f2b9', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(46, '2020-09-17 06:40:33', '2020-09-17 12:40:33', '638087cd63ed4c7b7d9ca51e33e6436197d6ee16', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(47, '2020-09-17 06:59:44', '2020-09-19 02:33:31', '5fd1146b3bae658b70a734c3422cb09086eaa9cc', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(48, '2020-09-17 07:10:35', '2020-09-17 07:26:15', '00424a968a5ea570d4c72b2957f9c7092ed99af3', 'admin', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 10; M2004J19C) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.81 Mobile Safari/537.36', 1),
(49, '2020-09-17 07:26:25', '2020-09-17 13:26:25', 'ab9ed9ebbd8549a694c8511eaffbe09b1acafd16', 'admin', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 10; M2004J19C) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.81 Mobile Safari/537.36', 1),
(50, '2020-09-19 02:33:45', '2020-09-19 08:33:45', '8df1fb13ffae343f7d5e9fe32aa5ffc0d50c8198', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(51, '2020-09-19 04:39:54', '2020-09-19 10:39:54', '1b6b5b7e0285f18d2777bc605b6e9695136b0671', 'admin', '127.0.0.1', 'Mozilla/5.0 (Linux; Android 10; M2004J19C) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.81 Mobile Safari/537.36', 1),
(52, '2020-09-24 12:51:12', '2020-09-24 12:51:18', '283d9f9e7dbde67b2cc9b5e836e0d4427d83216e', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(53, '2020-09-24 12:51:23', '2020-09-24 18:51:23', '5633f6f7b00d4ff70cd49ab1efd720b9311ba57f', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(54, '2020-09-24 13:02:36', '2020-09-24 19:02:36', '5aed07c5ff9f8f972df9d4f9db60b7797577b157', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(55, '2020-09-25 03:22:24', '2020-09-25 09:22:24', '6afae3310046b6dc004800c3c7efd7511c5a94ea', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(56, '2020-10-04 06:56:56', '2020-10-04 12:56:56', 'ed9e410c16e6aeb0da3b8863ae336472a0a00a0f', 'admin', '::1', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 1),
(57, '2020-10-04 07:26:56', '2020-10-04 08:41:20', 'a967a1cb0e4559aa6c1ebb733614f6687a487550', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(58, '2020-10-04 08:49:23', '2020-10-04 08:50:56', 'd995d106625a9dcd69bfb966864cffe1a2219d76', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(59, '2020-10-04 08:52:53', '2020-10-04 09:22:28', 'ba29062ac5e278d5a22274f55786d02b90506595', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(60, '2020-10-04 09:35:54', '2020-10-04 09:44:42', '37f23875239461d24391c1d6f45a7ce6bcfb536a', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(61, '2020-10-04 09:44:49', '2020-10-04 15:44:49', '2f0f7b07c159f920a5934e6d8d54595dcc23fe6f', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(62, '2020-10-04 17:00:54', '2020-10-04 18:21:08', '79693765f267d647685492701f70819c9d1b6937', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(63, '2020-10-04 18:22:25', '2020-10-04 18:22:28', 'c820084749a2d16cca7e02cba6c2c15c4ca245ec', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(64, '2020-10-04 18:25:42', '2020-10-05 00:25:42', '2fbe3d409938b78c7715f14a188174b2949f0788', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(65, '2020-10-05 02:47:07', '2020-10-05 08:47:07', '1803b61cab6958e1c35f3a29d988e2c6219cfad4', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(66, '2020-10-05 09:36:50', '2020-10-05 15:36:50', 'f43d384175c57e800a876ffa1db10a21d924232a', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(67, '2020-10-06 18:08:21', '2020-10-07 00:08:21', '3c090e04b135e7e5e3bfab9cde17069c2911f0d6', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(68, '2020-10-08 06:56:12', '2020-10-08 12:56:12', 'a54dc76158ba814725942cb15ce6e93d813edcca', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(69, '2020-10-09 18:28:07', '2020-10-10 00:28:07', '7228ec09143a12a36925dcbee8fb7c5cd39c890b', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1),
(70, '2020-10-11 13:57:01', '2020-10-11 14:17:14', 'e74238a5b0fead209a4f4416e82d197b2035a0b2', 'admin', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kas`
--

CREATE TABLE `tb_kas` (
  `id_kas` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `jumlah` int(128) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_kas`
--

INSERT INTO `tb_kas` (`id_kas`, `nama`, `jumlah`, `tanggal`) VALUES
(56, 'Alwan', 15000, '2020-06-26'),
(57, 'wadsad', 500000, '2019-12-12'),
(58, 'axaxaxj', 15000, '2020-06-19'),
(59, 'Nawla Unch', 15000, '2020-06-12'),
(60, 'jamal udin', 6000, '2020-06-01'),
(61, 'jamal udin', 15000, '2020-06-04'),
(62, 'jamal udin', 500000, '2020-06-05'),
(63, 'axaxaxj', 15000, '2018-09-01'),
(64, 'Jaenab', 7000, '2042-08-01'),
(65, 'Ukhtea', 9000, '2020-06-27'),
(66, 'jamal udin', 15000, '2020-06-27'),
(67, 'jamal', 8000, '2020-09-07'),
(68, 'minul', 500, '2020-06-27'),
(69, 'jaenal', 700000, '2020-07-04'),
(70, 'minul', 15000, '2020-06-19'),
(71, 'axaxaxj', 15000, '2020-07-01'),
(72, 'suki', 15000, '2020-07-07'),
(73, 'Muhammad Alfiandi', 50000, '2020-07-07'),
(74, 'Heeeeeesss', 9000, '2020-06-02'),
(75, 'hareudang', 500000, '2020-06-29'),
(76, 'admin', 9000, '2020-07-11'),
(77, 'adad', 500000, '2020-10-09'),
(78, 'muhaimin muhyi', 50000, '2020-10-05'),
(79, 'adad', 50000, '2020-10-04'),
(80, '&quot;select * ', 26262, '2020-09-30'),
(81, 'tes', 123, '2020-10-08'),
(82, 'sdasd', 1511, '2020-10-08'),
(83, 'sdasd', 125151, '2020-10-06'),
(84, 'sdbhsa', 515151, '2020-10-15'),
(85, 'minul', 6666, '2020-10-06'),
(86, 'nawla', 5555, '2020-10-08'),
(87, 'ksamdni', 45151, '2020-10-15'),
(88, 'wad', 1000, '2020-10-08'),
(89, 'hiii', 1000, '2020-10-06'),
(90, 'asdasd', 123123, '2020-10-07'),
(91, 'jbsadfb', 515145, '2020-10-15'),
(92, 'monyet', 100, '2020-10-16'),
(93, 'hai', 1200, '2020-10-16'),
(94, 'awds', 20, '2020-10-15'),
(95, 'jsgdyda', 2525, '2020-10-16'),
(96, 'jsgdyda', 2525, '2020-10-16'),
(97, 'sadsad', 51251, '2020-10-20'),
(98, 'awdawd', 5000, '2020-10-20');

-- --------------------------------------------------------

--
-- Table structure for table `tb_role`
--

CREATE TABLE `tb_role` (
  `id_role` int(11) NOT NULL,
  `name_role` varchar(50) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_role`
--

INSERT INTO `tb_role` (`id_role`, `name_role`, `is_active`) VALUES
(1, 'admin', 1),
(2, 'administrasi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `id_role`, `user`, `pass`, `level`) VALUES
(6, 1, 'dandi', 'dandi', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_admin_log`
--
ALTER TABLE `tb_admin_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_kas`
--
ALTER TABLE `tb_kas`
  ADD PRIMARY KEY (`id_kas`);

--
-- Indexes for table `tb_role`
--
ALTER TABLE `tb_role`
  ADD PRIMARY KEY (`id_role`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_role` (`id_role`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_admin_log`
--
ALTER TABLE `tb_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `tb_kas`
--
ALTER TABLE `tb_kas`
  MODIFY `id_kas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT for table `tb_role`
--
ALTER TABLE `tb_role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD CONSTRAINT `tb_user_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `tb_role` (`id_role`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
