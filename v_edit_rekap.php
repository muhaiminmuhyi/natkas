<?php
session_start();
if($_SESSION['id_role']==""){
	header("location:index?pesan=gagal");
}
if ( !isset($_SESSION['username'])) {
	header("location:index");
}
require 'conf/model.php';
$id_kas = $_GET['id'];
$model = new Model();
$data = $model->edit($id_kas);
?>
<!doctype html>
<html lang="en">
<head>
	<title>Edit</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assets/vendor/linearicons/style.css">
	<link rel="stylesheet" href="assets/vendor/chartist/css/chartist-custom.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<script src="https://kit.fontawesome.com/45da9d5b26.js" crossorigin="anonymous"></script>
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<?php include 'templates/navbar.php'; ?>
		<!-- END NAVBAR -->
		<!-- LEFT SIDEBAR -->
		<?php include "templates/sidebar.php"; ?>
		<!-- END LEFT SIDEBAR -->
		<!-- MAIN -->
		<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
          <!-- OVERVIEW -->
          <div class="panel">
            <div class="panel-heading">
              <h3 class="panel-title">Edit</h3>
            </div>
            <div class="panel-body">
              <form action="act/p_update_rkp?action=update" method="POST" onSubmit="validasi()">
              <input type="hidden" name="id_kas" value="<?php echo $data->id_kas ?>"/>

              <div class="input-group mb-3">
                <input type="text" class="form-control" id="nama" value="<?php echo $data->nama ?>" name="nama" aria-describedby="basic-addon1" <?php echo'readonly'?> >
              </div>
              <br>
              <div class="input-group mb-3">
                <input type="text" class="form-control" id="jumlah" onkeypress="return justNumber(event)" value="<?php echo $data->jumlah ?>" name="jumlah" aria-describedby="basic-addon1">
              </div>
              <br>
              <input type="date" name="tanggal" id="tanggal" value="<?php echo $data->tanggal ?>">
              <br>
              <br>
              <button type="submit" name="submit-up" class="btn btn-primary">UPDATE</button>
			  <button type="button" class="btn btn-secondary" onclick="window.location.href='v_rekap-kas'">Cancel</button>
              </form>
          </div>
        </div>
			</div>
			<!-- END MAIN CONTENT -->
		</div>
		<!-- END MAIN -->
		<div class="clearfix"></div>
    <?php include "templates/footer.php"; ?>
	</div>
	<!-- END WRAPPER -->
	<!-- Javascript -->
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
	<script src="assets/vendor/chartist/js/chartist.min.js"></script>
	<script src="assets/scripts/klorofil-common.js"></script>
	<script>
  function validasi()
	{
		var nama =	document.getElementById("nama").value;
		var jumlah = document.getElementById("jumlah").value;
		var tanggal = document.getElementById("nama").value;

		if (nama !="" && jumlah !="" && tanggal !="") {
			return true;
		} else {
			Swal.fire(
			'Good job!',
			'You clicked the button!',
			'success'
			);
		}
	}

  function justNumber(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))

		    return false;
		  return true;
		}

	$( function() {
    $( "#date" ).datepicker({
      dateFormat: "yy-mm-dd"
    });
  } );
	</script>
</body>

</html>
