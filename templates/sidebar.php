<div id="sidebar-nav" class="sidebar">
			<div class="sidebar-scroll">
				<nav>
					<ul class="nav">
						<li><a href="v_home" class="<?php if ($page == "dashboard"){echo 'active';} ?>"><i class="fas fa-tachometer-alt"></i> <span>Dashboard</span></a></li>
						<li><a href="v_rekap-kas" class="<?php if ($page == "rekap-kas"){echo 'active';} ?>"><i class="fas fa-plus"></i> <span>Add Data</span></a></li>
						<!-- <li><a href="charts.html" class=""><i class="lnr lnr-chart-bars"></i> <span>Charts</span></a></li>
						<li><a href="panels.html" class=""><i class="lnr lnr-cog"></i> <span>Panels</span></a></li>
						<li><a href="notifications.html" class=""><i class="lnr lnr-alarm"></i> <span>Notifications</span></a></li> -->
						<?php if ($_SESSION['id_role'] == 1) : ?>
						<li>
							<a href="#subPages" data-toggle="collapse" class="collapsed "><i class="fas fa-users"></i> <span>User Managements</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
							<div id="subPages" class="collapse ">
								<ul class="nav">
									<li><a href="v_users" class="<?php if($page == "v-user"){echo 'active';}?>">Users</a></li>
									<li><a href="v_role" class="<?php if($page == "v-role"){echo 'active';}?>">Role</a></li>
								</ul>
							</div>
						</li>
						<?php endif ?>
						<!-- <li><a href="tables.html" class=""><i class="lnr lnr-dice"></i> <span>Tables</span></a></li>
						<li><a href="typography.html" class=""><i class="lnr lnr-text-format"></i> <span>Typography</span></a></li>
						<li><a href="icons.html" class=""><i class="lnr lnr-linearicons"></i> <span>Icons</span></a></li> -->
					</ul>
				</nav>
			</div>
		</div>
